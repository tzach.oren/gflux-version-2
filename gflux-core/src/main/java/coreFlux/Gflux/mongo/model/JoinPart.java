package coreFlux.Gflux.mongo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class JoinPart {

    private final Map<String, Object> fetchedValue;

    private final String fieldName;

}
