package coreFlux.Gflux.mongo;

public class GFluxConstants {

    public static final String REF_ID_FIELD_NAME = "id";

    public static final String SUBSCRIPTION_TYPE_NAME = "Subscription";
}
