package coreFlux.Gflux.mongo;

import com.google.common.base.CaseFormat;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Function;

@Component
public class TypeNameToCollectionNameConvention implements Function<String, String> {

    @Override
    public String apply(String s) {
        return Optional.ofNullable(CaseFormat.UPPER_CAMEL.converterTo(CaseFormat.LOWER_UNDERSCORE).convert(s))
                .map(name -> name.replaceFirst("change_stream_", ""))
                .orElseThrow(() -> new IllegalArgumentException("Could not produce valid collection name for " + s));
    }
}
