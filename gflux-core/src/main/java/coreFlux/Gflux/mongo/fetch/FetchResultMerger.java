package coreFlux.Gflux.mongo.fetch;

import coreFlux.Gflux.mongo.GFluxConstants;
import coreFlux.Gflux.mongo.configurations.FieldNamesConventionConfiguration;
import coreFlux.Gflux.mongo.model.JoinPart;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

@AllArgsConstructor
public class FetchResultMerger implements Consumer<JoinPart> {

    private final Map<String, Object> currentState;

    private final FieldNamesConventionConfiguration fieldNamesConventionConfiguration;

    @Override
    public void accept(JoinPart joinPart) {

        String fieldName = joinPart.getFieldName();
        Object currentFieldValue = currentState.get(fieldName);
        String fetchedValueId = (String) joinPart.getFetchedValue().get(fieldNamesConventionConfiguration.getId());

        if (currentFieldValue == null) {
            currentState.put(fieldName, joinPart.getFetchedValue());
        } else if (currentFieldValue instanceof Collection) {
            Collection<Map<String, Object>> maps = (Collection<Map<String, Object>>) currentFieldValue;
            maps.stream()
                    .filter(element -> isMatchingId(element, fetchedValueId))
                    .findFirst()
                    .ifPresent(existingElement -> existingElement.putAll(joinPart.getFetchedValue()));
        } else if (currentFieldValue instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) currentFieldValue;
            map.putAll(joinPart.getFetchedValue());
        }
    }

    private boolean isMatchingId(Map<String, Object> entity, String id) {
        return Objects.equals(entity.get(GFluxConstants.REF_ID_FIELD_NAME), id);
    }
}
