package coreFlux.Gflux.mongo.fetch;

import com.mongodb.reactivestreams.client.MongoDatabase;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

import java.util.Objects;
import java.util.function.Predicate;

@AllArgsConstructor
public class SupportedCollectionsPredicate implements Predicate<String> {

    private final MongoDatabase mongoDatabase;

    @Override
    public boolean test(String testedCollectionName) {
        return Flux.from(mongoDatabase.listCollectionNames()).toStream().anyMatch(collectionName -> Objects.equals(collectionName, testedCollectionName));
    }
}
