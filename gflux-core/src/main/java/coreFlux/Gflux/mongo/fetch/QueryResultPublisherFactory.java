package coreFlux.Gflux.mongo.fetch;

import com.mongodb.Function;
import com.mongodb.reactivestreams.client.ChangeStreamPublisher;
import com.mongodb.reactivestreams.client.MongoCollection;
import coreFlux.Gflux.mongo.publisher.ChangeStreamPublisherAdapterProducer;
import coreFlux.Gflux.mongo.publisher.DocumentPublisherAdapterProducer;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.Collections;
import java.util.Map;

@AllArgsConstructor
public class QueryResultPublisherFactory {

    @NonNull
    private final ChangeStreamPublisherAdapterProducer<Document> changeStreamPublisherAdapterProducer;
    @NonNull
    private final DocumentPublisherAdapterProducer documentPublisherAdapterProducer;

    public Flux<Map<String, Object>> apply(@NonNull MongoCollection<Document> documentMongoCollection,
                                           @NonNull FetchMode fetchMode,
                                           @NonNull Bson filter) {
        return apply(documentMongoCollection, fetchMode,
                collection -> collection.find(filter),
                collection -> collection.watch(Collections.singletonList(filter)));
    }

    public Flux<Map<String, Object>> apply(@NonNull MongoCollection<Document> documentMongoCollection,
                                           @NonNull FetchMode fetchMode) {
        return apply(documentMongoCollection, fetchMode,
                MongoCollection::find,
                MongoCollection::watch);
    }

    private Flux<Map<String, Object>> apply(MongoCollection<Document> documentMongoCollection,
                                            FetchMode fetchMode,
                                            Function<MongoCollection<Document>, Publisher<Document>> dataStreamProducer,
                                            Function<MongoCollection<Document>, ChangeStreamPublisher<Document>> deltasStreamProducer) {
        if (FetchMode.CHANGE_STREAM.equals(fetchMode)) {
            return Flux.from(changeStreamPublisherAdapterProducer.apply(deltasStreamProducer.apply(documentMongoCollection)));
        } else if (FetchMode.INITIAL_LOAD.equals(fetchMode)) {
            return Flux.from(documentPublisherAdapterProducer.apply(dataStreamProducer.apply(documentMongoCollection)));
        }

        //The ordering of the calls to the changes publisher and the documents publisher is crucial.
        //Since we want to avoid losing changes - we must call "watch" before "find".
        //If we would have done the opposite, the changes which were performed after "find" but before "fetch" would be lost.
        Publisher<Map<String, Object>> deltasPublisher = changeStreamPublisherAdapterProducer.apply(deltasStreamProducer.apply(documentMongoCollection));
        Publisher<Map<String, Object>> dataPublisher = documentPublisherAdapterProducer.apply(dataStreamProducer.apply(documentMongoCollection));

        return Flux.concat(dataPublisher, deltasPublisher);
    }
}
