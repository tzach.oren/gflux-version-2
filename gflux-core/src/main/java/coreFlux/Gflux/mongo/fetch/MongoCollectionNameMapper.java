package coreFlux.Gflux.mongo.fetch;

import coreFlux.Gflux.mongo.GFluxConstants;
import coreFlux.Gflux.mongo.TypeNameToCollectionNameConvention;
import graphql.language.Field;
import graphql.language.FieldDefinition;
import graphql.language.ObjectTypeDefinition;
import graphql.schema.idl.TypeDefinitionRegistry;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

@AllArgsConstructor
public class MongoCollectionNameMapper implements Function<List<Field>, Optional<String>> {

    private final TypeDefinitionRegistry typeDefinitionRegistry;

    private final TypeNameToCollectionNameConvention typeNameToCollectionNameConvention;

    private final Predicate<String> supportedCollectionsPredicate;

    @Override
    public Optional<String> apply(List<Field> fields) {
        Optional<ObjectTypeDefinition> currentOptionalType = typeDefinitionRegistry.getType(GFluxConstants.SUBSCRIPTION_TYPE_NAME)
                .filter(ObjectTypeDefinition.class::isInstance)
                .map(ObjectTypeDefinition.class::cast);

        for (Field field : fields) {
            currentOptionalType = currentOptionalType.flatMap(currentType -> currentType.getFieldDefinitions()
                    .stream()
                    .filter(fieldDefinition -> fieldDefinition.getName().equals(field.getName()))
                    .findFirst()
                    .map(FieldDefinition::getType))
                    .flatMap(typeDefinitionRegistry::getType)
                    .filter(ObjectTypeDefinition.class::isInstance)
                    .map(ObjectTypeDefinition.class::cast);
        }

        return currentOptionalType
                .map(ObjectTypeDefinition::getName)
                .map(typeNameToCollectionNameConvention)
                .filter(supportedCollectionsPredicate);
    }
}
