package coreFlux.Gflux.mongo.fetch;

import com.mongodb.client.model.Filters;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import coreFlux.Gflux.mongo.GFluxConstants;
import coreFlux.Gflux.mongo.configurations.FieldNamesConventionConfiguration;
import coreFlux.Gflux.mongo.model.JoinPart;
import graphql.language.Field;
import graphql.schema.DataFetchingEnvironment;
import lombok.Builder;
import lombok.NonNull;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Builder
public class MongoDataFetcher {

    @NonNull
    private final MongoDatabase mongoDatabase;
    @NonNull
    private final FieldNamesConventionConfiguration fieldNamesConventionConfiguration;
    @NonNull
    private final QueryResultPublisherFactory queryResultPublisherFactory;
    @NonNull
    private final MongoCollectionNameMapper mongoCollectionNameMapper;


    public Flux<Map<String, Object>> fetch(@NonNull DataFetchingEnvironment environment,
                                           @NonNull String collectionName,
                                           @NonNull FetchMode fetchMode) {
        return fetch(environment, collectionName, (collection, factory) -> factory.apply(collection, fetchMode));
    }

    public Flux<Map<String, Object>> fetch(@NonNull DataFetchingEnvironment environment,
                                           @NonNull String collectionName,
                                           @NonNull FetchMode fetchMode,
                                           @NonNull Bson filter) {
        return fetch(environment, collectionName, (collection, factory) -> factory.apply(collection, fetchMode, filter));
    }


    private Flux<Map<String, Object>> fetch(DataFetchingEnvironment environment,
                                            String collectionName,
                                            BiFunction<MongoCollection<Document>, QueryResultPublisherFactory, Publisher<Map<String, Object>>> publisherCreationFunction) {
        MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);

        Field rootField = getRootField(environment, collectionName);

        return Flux.from(publisherCreationFunction.apply(collection, queryResultPublisherFactory))
                .concatMap(getJoinPublisher(Collections.singletonList(rootField)));
    }

    private Field getRootField(DataFetchingEnvironment dataFetchingEnvironment, String collectionName) {
        return dataFetchingEnvironment.getFields().stream()
                .filter(field -> field.getName().equalsIgnoreCase(collectionName))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    private Flux<JoinPart> getLinkedFieldPublisher(List<Field> fetchPath, Map<String, Object> element) {
        Field nextField = fetchPath.get(fetchPath.size() - 1);

        Optional<Bson> optionalInFilter = buildInFilter(element.get(nextField.getName()));

        if(!optionalInFilter.isPresent()) {
            return Flux.empty();
        }

        String mongoCollectionName = mongoCollectionNameMapper.apply(fetchPath).orElseThrow(IllegalArgumentException::new);

        MongoCollection<Document> collection = mongoDatabase.getCollection(mongoCollectionName);

        return Flux.from(queryResultPublisherFactory.apply(collection, FetchMode.INITIAL_LOAD, optionalInFilter.get()))
                .concatMap(getJoinPublisher(fetchPath))
                .map(result -> new JoinPart(result, nextField.getName()));
    }

    private Function<Map<String, Object>, Publisher<Map<String, Object>>> getJoinPublisher(List<Field> fetchPath) {
        return element -> {
            Field rootSelection = fetchPath.get(fetchPath.size() - 1);

            List<Flux<JoinPart>> joinPerformers = rootSelection.getSelectionSet()
                    .getSelections().stream()
                    .filter(Field.class::isInstance)
                    .map(Field.class::cast)
                    .map(field -> Stream.concat(fetchPath.stream(), Stream.of(field)).collect(Collectors.toList()))
                    .filter(nextFetchPath -> mongoCollectionNameMapper.apply(nextFetchPath).isPresent())
                    .map(nextFetchPath -> getLinkedFieldPublisher(nextFetchPath, element))
                    .collect(Collectors.toList());

            joinPerformers.forEach(joinPerformer -> joinPerformer.subscribe(new FetchResultMerger(element, fieldNamesConventionConfiguration)));

            return Flux.merge(joinPerformers).then(Mono.just(element));
        };
    }

    private Optional<Bson> buildInFilter(Object fetchedValueOfField) {
        if(fetchedValueOfField == null) {
            return Optional.empty();
        } else if(Map.class.isAssignableFrom(fetchedValueOfField.getClass())) {
            return Optional.of(Filters.in(fieldNamesConventionConfiguration.getId(),
                    new ObjectId(String.valueOf(((Map<String, Object>) fetchedValueOfField).get(GFluxConstants.REF_ID_FIELD_NAME)))));
        } else if(Collection.class.isAssignableFrom(fetchedValueOfField.getClass())) {
            return Optional.of(Filters.in(fieldNamesConventionConfiguration.getId(),
                    ((Collection<Map<String, Object>>) fetchedValueOfField).stream()
                            .map(e -> e.get(GFluxConstants.REF_ID_FIELD_NAME))
                            .map(String.class::cast)
                            .map(ObjectId::new)
                            .collect(Collectors.toList())));
        }

        return Optional.empty();
    }
}
