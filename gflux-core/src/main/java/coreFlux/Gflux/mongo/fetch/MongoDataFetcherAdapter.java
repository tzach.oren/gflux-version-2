package coreFlux.Gflux.mongo.fetch;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.Builder;
import lombok.NonNull;
import org.reactivestreams.Publisher;

import java.util.Map;

@Builder
public class MongoDataFetcherAdapter implements DataFetcher<Publisher<Map<String, Object>>> {

    @NonNull
    private final String collectionName;
    @NonNull
    private final MongoDataFetcher mongoDataFetcher;

    @Override
    public Publisher<Map<String, Object>> get(DataFetchingEnvironment environment) {
        return mongoDataFetcher.fetch(environment, collectionName, FetchMode.INITIAL_LOAD_AND_CHANGE_STREAM);
    }
}
