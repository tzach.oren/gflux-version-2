package coreFlux.Gflux.mongo.fetch;

import graphql.schema.DataFetcher;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.function.Function;

@AllArgsConstructor
public class MongoDataFetcherAdapterFactory implements Function<String, DataFetcher<?>> {

    @NonNull
    private final MongoDataFetcher mongoDataFetcher;

    @Override
    public DataFetcher<?> apply(String collectionName) {
        return MongoDataFetcherAdapter.builder()
                .collectionName(collectionName)
                .mongoDataFetcher(mongoDataFetcher)
                .build();
    }
}
