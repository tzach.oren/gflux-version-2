package coreFlux.Gflux.mongo.fetch;

public enum FetchMode {

    INITIAL_LOAD,
    CHANGE_STREAM,
    INITIAL_LOAD_AND_CHANGE_STREAM

}
