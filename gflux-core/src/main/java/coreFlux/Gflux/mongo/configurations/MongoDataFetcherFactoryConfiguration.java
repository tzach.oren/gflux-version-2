package coreFlux.Gflux.mongo.configurations;

import com.mongodb.MongoClientSettings;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import coreFlux.Gflux.mongo.fetch.MongoCollectionNameMapper;
import coreFlux.Gflux.mongo.fetch.MongoDataFetcher;
import coreFlux.Gflux.mongo.fetch.MongoDataFetcherAdapterFactory;
import coreFlux.Gflux.mongo.fetch.QueryResultPublisherFactory;
import coreFlux.Gflux.mongo.publisher.ChangeStreamPublisherAdapterProducer;
import coreFlux.Gflux.mongo.publisher.DocumentPublisherAdapterProducer;
import lombok.Setter;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "gflux.mongo")
@Setter
public class MongoDataFetcherFactoryConfiguration {

    private String connectionString;

    private String databaseName;

    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create(connectionString);
    }

    @Bean
    @Autowired
    public MongoDatabase mongoDatabase(MongoClient mongoClient) {
        CodecRegistry pojoCodecRegistry = CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        return mongoClient.getDatabase(databaseName).withCodecRegistry(codecRegistry);
    }

    @Bean
    @Autowired
    public QueryResultPublisherFactory queryResultPublisherFactory(ChangeStreamPublisherAdapterProducer<Document> changeStreamPublisherAdapterProducer,
                                                                   DocumentPublisherAdapterProducer documentPublisherAdapterProducer) {
        return new QueryResultPublisherFactory(changeStreamPublisherAdapterProducer, documentPublisherAdapterProducer);
    }

    @Bean
    @Autowired
    public MongoDataFetcher mongoDataFetcher(MongoDatabase mongoDatabase,
                                             QueryResultPublisherFactory queryResultPublisherFactory,
                                             MongoCollectionNameMapper mongoCollectionNameMapper,
                                             FieldNamesConventionConfiguration fieldNamesConventionConfiguration) {

        return MongoDataFetcher.builder()
                .mongoDatabase(mongoDatabase)
                .mongoCollectionNameMapper(mongoCollectionNameMapper)
                .queryResultPublisherFactory(queryResultPublisherFactory)
                .fieldNamesConventionConfiguration(fieldNamesConventionConfiguration)
                .build();
    }

    @Bean
    @Autowired
    public MongoDataFetcherAdapterFactory mongoDataFetcherFactory(MongoDataFetcher mongoDataFetcher) {
        return new MongoDataFetcherAdapterFactory(mongoDataFetcher);
    }
}
