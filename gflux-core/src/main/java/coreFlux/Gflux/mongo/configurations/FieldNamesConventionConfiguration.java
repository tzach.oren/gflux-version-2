package coreFlux.Gflux.mongo.configurations;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@ConfigurationProperties(prefix = "gflux.fields")
@Configuration
public class FieldNamesConventionConfiguration {

    private String id;

    private String changeStreamMetadata;

}
