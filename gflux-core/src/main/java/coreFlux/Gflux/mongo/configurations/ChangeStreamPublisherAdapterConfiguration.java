package coreFlux.Gflux.mongo.configurations;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import coreFlux.Gflux.mongo.publisher.ChangeStreamPublisherAdapterProducer;
import coreFlux.Gflux.mongo.publisher.DocumentPublisherAdapterProducer;
import coreFlux.Gflux.mongo.publisher.mappers.GsonMapper;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChangeStreamPublisherAdapterConfiguration {

    @Bean
    @Autowired
    public ChangeStreamPublisherAdapterProducer<Document> changeStreamPublisherAdapterProducer(GsonMapper<ChangeStreamDocument<Document>> changeStreamDocumentMapper) {
        return new ChangeStreamPublisherAdapterProducer<>(changeStreamDocumentMapper);
    }

    @Bean
    @Autowired
    public DocumentPublisherAdapterProducer documentPublisherAdapterProducer(GsonMapper<Document> documentMapper) {
        return new DocumentPublisherAdapterProducer(documentMapper);
    }
}
