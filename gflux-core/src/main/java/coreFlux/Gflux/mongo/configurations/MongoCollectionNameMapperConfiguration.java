package coreFlux.Gflux.mongo.configurations;

import com.mongodb.reactivestreams.client.MongoDatabase;
import coreFlux.Gflux.mongo.TypeNameToCollectionNameConvention;
import coreFlux.Gflux.mongo.fetch.MongoCollectionNameMapper;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.stream.Collectors;

@Configuration
public class MongoCollectionNameMapperConfiguration {

    @Autowired
    @Bean
    public MongoCollectionNameMapper mongoCollectionNameMapper(TypeNameToCollectionNameConvention typeNameToCollectionNameConvention,
                                                               TypeDefinitionRegistry typeDefinitionRegistry,
                                                               MongoDatabase mongoDatabase) {
        Set<String> existingCollectionNames = Flux.from(mongoDatabase.listCollectionNames()).toStream().collect(Collectors.toSet());
        return new MongoCollectionNameMapper(typeDefinitionRegistry, typeNameToCollectionNameConvention, existingCollectionNames::contains);
    }
}
