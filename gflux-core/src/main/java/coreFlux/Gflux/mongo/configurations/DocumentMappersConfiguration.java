package coreFlux.Gflux.mongo.configurations;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import coreFlux.Gflux.mongo.publisher.mappers.GsonMapper;
import coreFlux.Gflux.mongo.publisher.serialization.ChangeStreamDocumentSerializer;
import coreFlux.Gflux.mongo.publisher.serialization.ObjectIdSerializer;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DocumentMappersConfiguration {

    public static final String DOCUMENT_SERIALIZATION_GSON = "documentSerializationGson";

    @Bean(name = DOCUMENT_SERIALIZATION_GSON)
    public Gson documentSerializationGson() {
        return new GsonBuilder()
                .registerTypeAdapter(ObjectId.class, new ObjectIdSerializer())
                .registerTypeAdapter(ChangeStreamDocument.class, new ChangeStreamDocumentSerializer())
                .create();
    }

    @Bean
    @Autowired
    public GsonMapper<ChangeStreamDocument<Document>> changeStreamDocumentMapper(@Qualifier(value = DOCUMENT_SERIALIZATION_GSON) Gson gson) {
        return new GsonMapper<>(gson);
    }

    @Bean
    public GsonMapper<Document> documentMapper(@Qualifier(value = DOCUMENT_SERIALIZATION_GSON) Gson gson) {
        return new GsonMapper<>(gson);
    }
}
