package coreFlux.Gflux.mongo;

import coreFlux.Gflux.mongo.fetch.MongoDataFetcherAdapterFactory;
import graphql.language.*;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.TypeDefinitionRegistry;
import graphql.schema.idl.TypeRuntimeWiring;
import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.Optional;
import java.util.function.Consumer;

@AllArgsConstructor
public class RuntimeWiringTypesAppender implements Consumer<RuntimeWiring.Builder> {

    @NonNull
    private final TypeDefinitionRegistry typeDefinitionRegistry;

    @NonNull
    private final MongoDataFetcherAdapterFactory mongoDataFetcherAdapterFactory;

    @NonNull
    private final TypeNameToCollectionNameConvention typeNameToCollectionNameConvention;

    @Override
    public void accept(RuntimeWiring.Builder builder) {
        typeDefinitionRegistry.getType(GFluxConstants.SUBSCRIPTION_TYPE_NAME).ifPresent(typeDefinition -> {
            typeDefinition.getChildren()
                    .stream()
                    .filter(FieldDefinition.class::isInstance)
                    .map(FieldDefinition.class::cast)
                    .forEach(fieldDefinition -> {
                        String fieldTypeName = getTypeName(fieldDefinition.getType()).orElseThrow(IllegalAccessError::new);
                        String collectionName = typeNameToCollectionNameConvention.apply(fieldTypeName);
                        builder.type(TypeRuntimeWiring.newTypeWiring(GFluxConstants.SUBSCRIPTION_TYPE_NAME)
                                .dataFetcher(fieldDefinition.getName(), mongoDataFetcherAdapterFactory.apply(collectionName)));
                    });
        });
    }

    private Optional<String> getTypeName(Type type) {
        if (type instanceof TypeName) {
            return Optional.of(((TypeName) type).getName());
        } else if (type instanceof NonNullType) {
            return getTypeName(((NonNullType) type).getType());
        } else if (type instanceof ListType) {
            return getTypeName(((ListType) type).getType());
        }

        return Optional.empty();
    }
}
