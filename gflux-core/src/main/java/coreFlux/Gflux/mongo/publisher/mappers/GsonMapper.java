package coreFlux.Gflux.mongo.publisher.mappers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.function.Function;

@RequiredArgsConstructor
public class GsonMapper<T> implements Function<T, Map<String, Object>> {

    private static final Type deserializationTargetType = new TypeToken<Map<String, Object>>() {
    }.getType();

    @NonNull
    private final Gson gson;

    @Override
    public Map<String, Object> apply(T t) {
        return gson.fromJson(gson.toJsonTree(t), deserializationTargetType);
    }
}
