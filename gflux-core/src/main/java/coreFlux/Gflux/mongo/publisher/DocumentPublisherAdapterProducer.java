package coreFlux.Gflux.mongo.publisher;

import coreFlux.Gflux.mongo.publisher.mappers.GsonMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.function.Function;

@RequiredArgsConstructor
public class DocumentPublisherAdapterProducer implements Function<Publisher<Document>, Publisher<Map<String, Object>>> {

    @NonNull
    private final GsonMapper<Document> documentMapper;

    @Override
    public Publisher<Map<String, Object>> apply(Publisher<Document> documentPublisher) {
        return Flux.from(documentPublisher).map(documentMapper);
    }
}
