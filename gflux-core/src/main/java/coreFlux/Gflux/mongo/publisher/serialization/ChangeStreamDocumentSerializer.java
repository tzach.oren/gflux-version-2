package coreFlux.Gflux.mongo.publisher.serialization;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.mongodb.client.model.changestream.ChangeStreamDocument;
import org.bson.BsonInt64;
import org.bson.BsonTimestamp;

import java.lang.reflect.Type;
import java.util.Optional;

public class ChangeStreamDocumentSerializer implements JsonSerializer<ChangeStreamDocument<?>> {

    @Override
    public JsonElement serialize(ChangeStreamDocument<?> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = context.serialize(src.getFullDocument()).getAsJsonObject();

        JsonObject changeStreamMetadata = new JsonObject();
        result.add("changeStreamMetadata", changeStreamMetadata);

        changeStreamMetadata.add("resumeToken", context.serialize(src.getResumeToken()));
        changeStreamMetadata.add("namespaceDocument", context.serialize(src.getNamespaceDocument()));
        changeStreamMetadata.add("destinationNamespaceDocument", context.serialize(src.getDestinationNamespace()));
        changeStreamMetadata.add("documentKey", context.serialize(src.getDocumentKey()));
        changeStreamMetadata.add("clusterTime", context.serialize(Optional.ofNullable(src.getClusterTime()).map(BsonTimestamp::getValue).orElse(null)));
        changeStreamMetadata.add("operationType", context.serialize(src.getOperationType()));
        changeStreamMetadata.add("updateDescription", context.serialize(src.getUpdateDescription()));
        changeStreamMetadata.add("txnNumber", context.serialize(Optional.ofNullable(src.getTxnNumber()).map(BsonInt64::getValue).orElse(null)));
        changeStreamMetadata.add("lsid", context.serialize(src.getLsid()));

        return result;
    }

}
