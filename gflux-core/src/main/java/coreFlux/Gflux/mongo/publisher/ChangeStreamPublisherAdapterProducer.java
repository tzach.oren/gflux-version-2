package coreFlux.Gflux.mongo.publisher;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.reactivestreams.client.ChangeStreamPublisher;
import coreFlux.Gflux.mongo.publisher.mappers.GsonMapper;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.function.Function;

@AllArgsConstructor
public class ChangeStreamPublisherAdapterProducer<T> implements Function<ChangeStreamPublisher<T>, Publisher<Map<String, Object>>> {

    @NonNull
    private final GsonMapper<ChangeStreamDocument<T>> changeStreamDocumentMapper;

    @Override
    public Publisher<Map<String, Object>> apply(ChangeStreamPublisher<T> changeStreamPublisher) {
        return Flux.from(changeStreamPublisher).map(changeStreamDocumentMapper);
    }
}
