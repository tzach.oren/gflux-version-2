package coreFlux.Gflux.graphql.configurations;

import coreFlux.Gflux.mongo.fetch.MongoDataFetcherAdapterFactory;
import coreFlux.Gflux.mongo.RuntimeWiringTypesAppender;
import coreFlux.Gflux.mongo.TypeNameToCollectionNameConvention;
import graphql.GraphQL;
import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.tracing.TracingInstrumentation;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;

@Configuration
public class GraphQLConfiguration {

    @Value("${gflux.graphql.schema}")
    private String graphQLSchema;

    @Bean
    @Autowired
    public GraphQLSchema buildSchema(TypeDefinitionRegistry typeDefinitionRegistry, RuntimeWiringTypesAppender runtimeWiringTypesAppender) {
        //
        // reads a file that provides the schema types
        //
        RuntimeWiring.Builder wiring = RuntimeWiring.newRuntimeWiring();
        runtimeWiringTypesAppender.accept(wiring);

        return new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, wiring.build());
    }

    private Reader loadSchemaFile() {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(graphQLSchema);
        return new InputStreamReader(stream);
    }

    @Bean
    @Autowired
    public RuntimeWiringTypesAppender runtimeWiringTypesAppender(TypeDefinitionRegistry typeDefinitionRegistry,
                                                                 MongoDataFetcherAdapterFactory mongoDataFetcherAdapterFactory,
                                                                 TypeNameToCollectionNameConvention typeNameToCollectionNameConvention) {
        return new RuntimeWiringTypesAppender(typeDefinitionRegistry, mongoDataFetcherAdapterFactory, new TypeNameToCollectionNameConvention());
    }

    @Bean
    public TypeDefinitionRegistry typeDefinitionRegistry() {
        return new SchemaParser().parse(loadSchemaFile());
    }

    @Bean
    @Autowired
    public GraphQL graphQL(GraphQLSchema graphQLSchema) {
        return GraphQL.newGraphQL(graphQLSchema).instrumentation(instrumentation()).build();
    }

    @Bean
    public Instrumentation instrumentation() {
        return new ChainedInstrumentation(
                Collections.singletonList(new TracingInstrumentation())
        );
    }
}
