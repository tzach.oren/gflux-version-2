package coreFlux.Gflux.graphql.ws.protocol;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GqlWsRequest {

    private final GqlWsMessageType type;

    private final String id;

    private final GqlWsRequestPayload payload;
}
