package coreFlux.Gflux.graphql.ws.protocol;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GqlWsResponse {

    private String id;

    private GqlWsMessageType type;

    private GqlWsResponsePayload payload;

}
