package coreFlux.Gflux.graphql.ws.protocol;

import com.google.gson.annotations.SerializedName;

public enum GqlWsMessageType {

    @SerializedName(value = "connection_init", alternate = {"gql_connection_init", "CONNECTION_INIT"})
    CONNECTION_INIT,

    @SerializedName(value = "connection_ack", alternate = {"gql_connection_ack", "CONNECTION_ACK"})
    CONNECTION_ACK,

    @SerializedName(value = "start", alternate = {"gql_start", "START"})
    START,

    @SerializedName(value = "stop", alternate = {"gql_stop", "STOP"})
    STOP,

    @SerializedName(value = "connection_terminate", alternate = {"gql_connection_terminate", "CONNECTION_TERMINATE"})
    CONNECTION_TERMINATE,

    @SerializedName(value = "connection_error", alternate = {"gql_connection_error", "CONNECTION_ERROR"})
    CONNECTION_ERROR,

    @SerializedName(value = "data", alternate = {"gql_data", "DATA"})
    DATA,

    @SerializedName(value = "error", alternate = {"gql_error", "ERROR"})
    ERROR,

    @SerializedName(value = "complete", alternate = {"gql_complete", "COMPLETE"})
    COMPLETE,

    @SerializedName(value = "connection_keep_alive", alternate = {"gql_connection_keep_alive", "CONNECTION_KEEP_ALIVE"})
    CONNECTION_KEEP_ALIVE
}
