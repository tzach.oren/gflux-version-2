package coreFlux.Gflux.graphql.ws;

import com.google.gson.Gson;
import coreFlux.Gflux.graphql.ws.subscribers.TerminatingGqlWsExecutionResultSubscriber;
import coreFlux.Gflux.graphql.ws.protocol.*;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.SubProtocolCapable;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;

@Slf4j
@Component
public class GqlWebSocketHandler extends TextWebSocketHandler implements SubProtocolCapable {

    @Autowired
    private GraphQL graphQL;

    @Autowired
    private ResponseMessagesSender responseMessagesSender;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        Gson gson = new Gson();
        GqlWsRequest request = gson.fromJson(message.getPayload(), GqlWsRequest.class);

        responseMessagesSender.sendResponses(session, getPreExecutionResponseMessages(request));
        executeQuery(session, request);
    }

    private List<GqlWsResponse> getPreExecutionResponseMessages(GqlWsRequest request) {
        List<GqlWsResponse> result = new LinkedList<>();

        if (GqlWsMessageType.CONNECTION_INIT.equals(request.getType())) {
            result.add(GqlWsResponse.builder()
                    .id(request.getId())
                    .type(GqlWsMessageType.CONNECTION_ACK)
                    .build());

            result.add(GqlWsResponse.builder()
                    .id(request.getId())
                    .type(GqlWsMessageType.CONNECTION_KEEP_ALIVE)
                    .build());
        } else if (GqlWsMessageType.START.equals(request.getType())) {
            result.add(GqlWsResponse.builder()
                    .id(request.getId())
                    .type(GqlWsMessageType.CONNECTION_KEEP_ALIVE)
                    .build());
        } else if (GqlWsMessageType.STOP.equals(request.getType())) {
            result.add(GqlWsResponse.builder()
                    .id(request.getId())
                    .type(GqlWsMessageType.COMPLETE)
                    .build());
        }

        return result;
    }

    private void executeQuery(WebSocketSession webSocketSession, GqlWsRequest request) throws IOException {
        if (!GqlWsMessageType.START.equals(request.getType())) {
            return;
        }

        GqlWsRequestPayload parameters = request.getPayload();

        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
                .query(parameters.getQuery())
                .variables(Optional.ofNullable(parameters.getVariables()).orElse(Collections.emptyMap()))
                .operationName(parameters.getOperationName())
                .build();

        ExecutionResult executionResult = graphQL.execute(executionInput);

        if (executionResult.getErrors().isEmpty()) {
            Publisher<ExecutionResult> resultStream = executionResult.getData();

            resultStream.subscribe(new TerminatingGqlWsExecutionResultSubscriber(responseMessagesSender, webSocketSession, request.getId(), 1));
        } else {
            responseMessagesSender.sendResponse(webSocketSession, GqlWsResponse.builder()
                    .id(request.getId())
                    .type(GqlWsMessageType.ERROR)
                    .payload(GqlWsResponsePayload.builder()
                            .errors(executionResult.getErrors().toArray())
                            .build())
                    .build());
        }
    }

    @Override
    public List<String> getSubProtocols() {
        return singletonList("graphql-ws");
    }
}
