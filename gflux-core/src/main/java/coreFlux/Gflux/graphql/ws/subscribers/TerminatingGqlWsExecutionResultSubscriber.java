package coreFlux.Gflux.graphql.ws.subscribers;

import coreFlux.Gflux.graphql.ws.ResponseMessagesSender;
import coreFlux.Gflux.graphql.ws.protocol.GqlWsMessageType;
import coreFlux.Gflux.graphql.ws.protocol.GqlWsResponse;
import coreFlux.Gflux.graphql.ws.protocol.GqlWsResponsePayload;
import graphql.ExecutionResult;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
public class TerminatingGqlWsExecutionResultSubscriber implements Subscriber<ExecutionResult> {

    @NonNull
    private final ResponseMessagesSender responseMessagesSender;
    @NonNull
    private final WebSocketSession webSocketSession;
    @NonNull
    private final String requestId;
    private final int requestedItemsAmount;
    private Subscription subscription;

    @Override
    public void onSubscribe(Subscription s) {
        this.subscription = s;
        requestNextItems();
    }

    @Override
    public void onNext(ExecutionResult er) {
        log.debug("Sending update");
        try {
            Object mongoUpdate = er.getData();
            responseMessagesSender.sendResponse(webSocketSession, GqlWsResponse.builder()
                    .type(GqlWsMessageType.DATA)
                    .id(requestId)
                    .payload(GqlWsResponsePayload.builder()
                            .data(mongoUpdate)
                            .build())
                    .build());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        requestNextItems();
    }

    @Override
    public void onError(Throwable t) {
        log.error("Subscription threw an exception", t);
        try {
            responseMessagesSender.sendResponse(webSocketSession, GqlWsResponse.builder()
                    .id(requestId)
                    .type(GqlWsMessageType.ERROR)
                    .payload(GqlWsResponsePayload.builder()
                            .errors(new Object[]{t.getMessage()})
                            .build())
                    .build());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onComplete() {
        log.info("Subscription complete");
        try {
            responseMessagesSender.sendResponse(webSocketSession,
                    GqlWsResponse.builder()
                            .id(requestId)
                            .type(GqlWsMessageType.COMPLETE)
                            .build());
            requestNextItems();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void requestNextItems() {
        if (subscription != null) {
            subscription.request(requestedItemsAmount);
        }
    }
}
