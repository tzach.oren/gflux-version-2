package coreFlux.Gflux.graphql.ws.protocol;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class GqlWsRequestPayload {

    private final String query;

    private final String operationName;

    private final Map<String, Object> variables;

}
