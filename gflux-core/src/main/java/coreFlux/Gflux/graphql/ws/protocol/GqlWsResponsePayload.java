package coreFlux.Gflux.graphql.ws.protocol;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GqlWsResponsePayload {

    private final Object data;

    private final Object[] errors;

}
