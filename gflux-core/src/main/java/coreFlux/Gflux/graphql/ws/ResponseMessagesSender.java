package coreFlux.Gflux.graphql.ws;

import coreFlux.Gflux.graphql.ws.protocol.GqlWsResponse;
import coreFlux.Gflux.tools.JsonKit;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Collection;

@Component
public class ResponseMessagesSender {

    public void sendResponses(WebSocketSession webSocketSession, Collection<GqlWsResponse> responses) throws IOException {
        for (GqlWsResponse response : responses) {
            sendResponse(webSocketSession, response);
        }
    }

    public void sendResponse(WebSocketSession webSocketSession, GqlWsResponse response) throws IOException {
        webSocketSession.sendMessage(new TextMessage(JsonKit.toJsonString(response)));
    }
}
