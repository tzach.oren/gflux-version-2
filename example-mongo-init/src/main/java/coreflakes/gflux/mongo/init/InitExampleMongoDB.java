package coreflakes.gflux.mongo.init;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

/**
 *  Will Run over each file in resource/data
 *  for each file will create collection with his name and will use the data from the JsonArray
 *  Arguments on launch
 *
 *  arg[0] - connection string (default: mongodb://127.0.0.1)
 *  arg[1] - database (default: tests)
 */
public class InitExampleMongoDB {

    public static void main(String[] args) throws URISyntaxException {
        String connectionString;
        String db;

        if(args.length > 0) {
            connectionString = args[0];
        } else {
            connectionString = "mongodb://localhost:27017/?replicaSet=rs0&readPreference=primary&appname=MongoDB&ssl=false";
        }

        if(args.length > 1) {
            db = args[1];
        } else {
            db = "tests";
        }

        JsonParser jsonParser = new JsonParser();

        try (MongoClient mongoClient = MongoClients.create(connectionString)){
            MongoDatabase database = mongoClient.getDatabase(db);

            cleanDB(database);
            initDBFromFiles(database, jsonParser);
        }
    }

    private static void cleanDB(MongoDatabase database) {
        database.drop();
    }

    private static void initDBFromFiles(MongoDatabase database, JsonParser jsonParser) throws URISyntaxException {
        URL url = InitExampleMongoDB.class.getClassLoader().getResource("data/");

        File resourceDataFolder = Paths.get(url.toURI()).toFile();

        for (File collectionFile: resourceDataFolder.listFiles()) {
            initDBCollection(database, collectionFile, jsonParser);
        }

    }

    private static void initDBCollection(MongoDatabase database, File file, JsonParser jsonParser){
        MongoCollection<Document> mongoCollection = database.getCollection(removeExtension(file.getName()));

        JsonArray jsonArray = readFile(file, jsonParser);

        List<Document> documents = new LinkedList<>();
        for (JsonElement jsonElement : jsonArray) {
            documents.add(Document.parse(jsonElement.toString()));
        }

        mongoCollection.insertMany(documents);
    }

    private static JsonArray readFile(File file, JsonParser jsonParser)  {
        try(FileReader fileReader = new FileReader(file)) {
            return jsonParser.parse(fileReader).getAsJsonArray();
        } catch (IOException e) {
            System.out.println(e);
            return new JsonArray();
        }
    }

    private static String removeExtension(String filename){
        return  filename.replaceFirst("[.][^.]+$", "");
    }

}
